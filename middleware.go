package base

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"os"
)


var DefaultJWTConfig = middleware.JWTConfig{
	Claims:     &JWTCustomClaims{},
	SigningKey: []byte(os.Getenv("MIDDLEWARE_SIGNING_KEY")),
}

func NoMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if err := next(c); err != nil {
			c.Error(err)
		}
		return nil
	}
}

func OnlyAdminMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {

		user := c.Get("user").(*jwt.Token)
		claims := user.Claims.(*JWTCustomClaims)
		if claims.Admin != true {
			return echo.ErrForbidden
		}
		if err := next(c); err != nil {
			c.Error(err)
		}
		return nil
	}
}
