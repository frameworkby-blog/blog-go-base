package base

import (
	"errors"
	"fmt"
	"log"

	"github.com/globalsign/mgo/bson"
	"github.com/thedevsaddam/govalidator"
)

func init() {
	govalidator.AddCustomRule("object_id", func(field string, rule string, message string, value interface{}) error {
		id := value.(string)
		log.Println(id)
		if !bson.IsObjectIdHex(id) {
			if message != "" {
				return errors.New(message)
			}
			return fmt.Errorf("the %s field must be valid ObjectId", field)
		}
		return nil
	})
}
