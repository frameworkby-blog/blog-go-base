package base

import (
	"github.com/go-bongo/bongo"
	"os"
)

var Connection = &bongo.Connection{}

func init() {
	config := &bongo.Config{
		ConnectionString: os.Getenv("MONGODB_CONNECTION_STRING"),
		Database:         os.Getenv("MONGODB_DATABASE"),
	}
	connection, err := bongo.Connect(config)
	if err != nil {
		panic(err)
	}
	Connection = connection
}

func Collection(name string) *bongo.Collection {
	return Connection.Collection(name)
}
