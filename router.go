package base

import (
	"github.com/labstack/echo"
)

type Route struct {
	Name           string
	Method         string
	Pattern        string
	HandlerFunc    echo.HandlerFunc
	MiddlewareFunc []echo.MiddlewareFunc
}

type Routes []Route

func NewRouter(group string, routes Routes, e *echo.Echo) {
	g := e.Group(group)

	for _, route := range routes {
		switch route.Method {
		case "GET":
			g.GET(route.Pattern, route.HandlerFunc, route.MiddlewareFunc...)
		case "POST":
			g.POST(route.Pattern, route.HandlerFunc, route.MiddlewareFunc...)
		}
	}
}

var handler = &Handler{}
var CommonRoutes = Routes{
	Route{
		Name:           "GetAll",
		Method:         "GET",
		Pattern:        "ping",
		HandlerFunc:    handler.HealthCheck,
		MiddlewareFunc: []echo.MiddlewareFunc{NoMiddleware},
	},
}
