package base

import (
	"net/http"

	"github.com/labstack/echo"
)

type Handler struct {
}

func (h *Handler) HealthCheck(c echo.Context) error {
	return c.JSON(http.StatusOK, echo.Map{"message": "pong"})
}
