package base

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/globalsign/mgo/bson"
)

type JWTCustomClaims struct {
	Login  string        `json:"login"`
	UserID bson.ObjectId `json:"user_id"`
	Admin  bool          `json:"admin"`
	jwt.StandardClaims
}
