package base

type Error struct {
	Code int `json:"code"`
	Message string `json:"message"`
}

var (
	ErrorNotFound = Error{ Code: 404, Message: "Document not found"}
)
